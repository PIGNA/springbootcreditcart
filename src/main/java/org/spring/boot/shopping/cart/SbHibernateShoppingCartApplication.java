package org.spring.boot.shopping.cart;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { //
		DataSourceAutoConfiguration.class, //
		DataSourceTransactionManagerAutoConfiguration.class, //
		HibernateJpaAutoConfiguration.class,
		ErrorMvcAutoConfiguration.class
		})
@ComponentScan({"org.spring.boot.shopping.cart.trainning.SpringBootTrainnig.dao",
"org.spring.boot.shopping.cart.trainning.SpringBootTrainnig.service"})
public class SbHibernateShoppingCartApplication {
//ErrorMvcAutoConfiguration.class
	public static void main(String[] args) {
		SpringApplication.run(SbHibernateShoppingCartApplication.class, args);
	}
	@Autowired(required = true)
	private Environment env;

	@Autowired(required = true)
	@Bean
    public LocalSessionFactoryBean sessionFactory() {
		
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("org.spring.boot.shopping.cart.trainning.SpringBootTrainnig.dao",
        		"org.spring.boot.shopping.cart.trainning.SpringBootTrainnig.service");
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
        hibernateProperties.put("hibernate.show_sql","true");
        hibernateProperties.put("current_session_context_class","org.springframework.orm.hibernate5.SpringSessionContext");
        sessionFactory.setHibernateProperties(hibernateProperties);
        return sessionFactory;
    }
	
	//@Autowired(required = true)
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("org.postgresql.Driver","org.postgresql.Driver"));
		dataSource.setUrl(env.getProperty("jdbc:postgresql://localhost:5434/CreditCard","jdbc:postgresql://localhost:5434/CreditCard"));
		dataSource.setUsername(env.getProperty("postgres","postgres"));
		dataSource.setPassword(env.getProperty("cheriecarole1","cheriecarole1"));
        return dataSource;
    }
    
	@Autowired(required = true)
    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory().getObject());
        return txManager;
    }
	
}
