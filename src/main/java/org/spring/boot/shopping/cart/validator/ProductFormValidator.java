package org.spring.boot.shopping.cart.validator;

import org.spring.boot.shopping.cart.dao.ProductDAO;
import org.spring.boot.shopping.cart.form.ProductForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProductFormValidator implements Validator {

	@Autowired ProductDAO productDao;
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == ProductForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ProductForm productForm = (ProductForm) target;
		
		//check the field of product
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.productForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "NotEmpty.productForm.code");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty.productForm.price");
		
		String code = productForm.getCode();
		
		if(code != null && code.length()>0) {
			if(code.matches("\\s+")) {//one or more white space(regex)
				errors.rejectValue("code", "Pattern.productForm.code");
			} else if(productForm.isNewProduct()) {
				
			}
		}
	}

}
