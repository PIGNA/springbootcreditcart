package org.spring.boot.shopping.cart.confing;

import org.spring.boot.shopping.cart.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class WebSecurityConfing extends WebSecurityConfigurerAdapter{

	@Autowired(required = true)
	UserDetailsServiceImpl userDetailsService;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		//Setting Service to find User in database and setting passwordEncoder
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		
		//requires login with ROLE_EMPLOYEE or ROLE_MANAGER
		//if not it will redirect to admin/login
		http.authorizeRequests().antMatchers("/admin/orderList","/admin/order","/admin/accountInfo")//
		.access("hasRole('ROLE_EMPLOYEE','ROLE_MANAGER')");
		
		//Pages only for MANAGER
		http.authorizeRequests().antMatchers("/admin/product").access("hasRole('ROLE_MANAGER')");
		
		//When role user, role XX
		//But access to the page requires role YY
		// An exception is thrown
		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

		//Configuration for login form
		http.authorizeRequests().and().formLogin()//
		//
		.loginProcessingUrl("/j_spring_security_check")//the url to submit the username and password to
		.loginPage("/admin/login")//the custom login page
		.defaultSuccessUrl("/admin/accountInfo")//the landing page after a successful login
		.failureUrl("/admin/login?error=true")//
		.usernameParameter("userName")//
		.passwordParameter("password")
		
		//Configuration for the Logout page.
		// (After logout, go to the home page)
		.and().logout().logoutUrl("/admin/logout").logoutSuccessUrl("/");
	}
}
