package org.spring.boot.shopping.cart.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.spring.boot.shopping.cart.form.CustomerForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CustomerFormValidator implements Validator {

	private EmailValidator emailValidator = EmailValidator.getInstance();

	//This validator only cheks for he CustomerForm
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == CustomerForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		CustomerForm customerForm = (CustomerForm) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.customerForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.customerForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.customerForm.phone");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty.customerForm.address");
		
		if(!emailValidator.isValid(customerForm.getEmail())) {
			errors.rejectValue("email", "Pattern.customerForm.email");
		}
	}
	 
	//Only 

	
}
